#!/usr/bin/env python3

import discord
import typing
from discord.ext import commands

bot = commands.Bot(command_prefix='!')
bot.remove_command('help')

# Nom des role
strInfo = "Info"
strInfoG1 = "Info-G1"
strInfoG2 = "Info-G2"
strMiage = "Miage"
strMiageG1 = "Miage-G1"
strMiageG2 = "Miage-G2"


#Set l'activite du bot 
@bot.event
async def on_ready():
    activite = discord.Streaming(name="""!help pour voir l'aide""",url="""https://gitlab.com/lamisedaxeh/bot_discord_l3""",details="""Demi-Vie-III""",twitch_name="""|_3""" );
    await bot.change_presence(activity=activite)



# Have fun
@bot.command()
async def ping(ctx):
    await ctx.send('pong')

# Commande Help et pour les commandes
@bot.command()
async def help(ctx):
    await ctx.send(""">>> **!info [g1 ou g2]** 
              Ajoute au groupe Info
              [g1 ou g2] argument optionnel pour le groupe Info-G1 ou Info-G2""")
    await ctx.send(""">>> **!miage [g1 ou g2]** 
              Ajoute au groupe Miage
              [g1 ou g2] argument optionnel pour le groupe Miage-G1 ou Miage-G2""")
    await ctx.send(""">>> **!information** 
                  divers liens utiles""")
    await ctx.send(""">>> **Info :**
      Code source du projet : https://gitlab.com/lamisedaxeh/bot_discord_l3""")
    

# Commande lien utile
@bot.command()
async def information(ctx):
    await ctx.send(""">>> **Stage - Furst : ** https://home.mis.u-picardie.fr/~furst/stage.php""")
    await ctx.send(""">>> **Aid'e à la détection d'erreurs  - Vincent Villain :** https://home.mis.u-picardie.fr/~villain/L3_ADE/""")
    await ctx.send(""">>> **Langage Formel - Vincent Villain :** https://home.mis.u-picardie.fr/~villain/L3_LF/""")
    await ctx.send(""">>> ** Programmation des Systèmes d'Information - Gaël Lemahec :** https://home.mis.u-picardie.fr/~lemahec/PSI/doku.php""")

# Commande Setup role Info + Info groupe
#   @ctx context du message initial
#   @agr argument optionnel avec soit g1 ou g2
@bot.command()
async def info(ctx,agr: typing.Optional[str] = None):
    # Récupération des roles
    info = discord.utils.get(ctx.message.channel.guild.roles, name=strInfo) 
    infoG1 = discord.utils.get(ctx.message.channel.guild.roles, name=strInfoG1) 
    infoG2 = discord.utils.get(ctx.message.channel.guild.roles, name=strInfoG2) 
    miage = discord.utils.get(ctx.message.channel.guild.roles, name=strMiage) 
    miageG1 = discord.utils.get(ctx.message.channel.guild.roles, name=strMiageG1) 
    miageG2 = discord.utils.get(ctx.message.channel.guild.roles, name=strMiageG2) 
    
    #Supp les roles 
    await ctx.message.author.remove_roles(info)
    await ctx.message.author.remove_roles(infoG1)
    await ctx.message.author.remove_roles(infoG2)
    await ctx.message.author.remove_roles(miage)
    await ctx.message.author.remove_roles(miageG1)
    await ctx.message.author.remove_roles(miageG2)

    #Ajoute le role info
    await ctx.message.author.add_roles(info)

    #Ajoute le role Info-G1 ou Info-G2
    if agr:
        if agr == "g1":
            await ctx.message.author.add_roles(infoG1)
        elif agr == "g2":
            await ctx.message.author.add_roles(infoG2)

    await ctx.send(ctx.message.author.name + " a été ajouté au groupe Info")


# Commande Setup role Miage + Miage groupe
#   @ctx context du message initial
#   @agr argument optionnel avec soit g1 ou g2
@bot.command()
async def miage(ctx,agr: typing.Optional[str] = None):
    # Récupération des roles
    miage = discord.utils.get(ctx.message.channel.guild.roles, name=strMiage) 
    miageG1 = discord.utils.get(ctx.message.channel.guild.roles, name=strMiageG1) 
    miageG2 = discord.utils.get(ctx.message.channel.guild.roles, name=strMiageG2) 
    info = discord.utils.get(ctx.message.channel.guild.roles, name=strInfo) 
    infoG1 = discord.utils.get(ctx.message.channel.guild.roles, name=strInfoG1) 
    infoG2 = discord.utils.get(ctx.message.channel.guild.roles, name=strInfoG2) 
    
    #Supp les roles 
    await ctx.message.author.remove_roles(miage)
    await ctx.message.author.remove_roles(miageG1)
    await ctx.message.author.remove_roles(miageG2)
    await ctx.message.author.remove_roles(info)
    await ctx.message.author.remove_roles(infoG1)
    await ctx.message.author.remove_roles(infoG2)

    #Ajoute le role miage
    await ctx.message.author.add_roles(miage)

    #Ajoute le role Miage-G1 ou Miage-G2
    if agr:
        if agr == "g1":
            await ctx.message.author.add_roles(miageG1)
        elif agr == "g2":
            await ctx.message.author.add_roles(miageG2)

    await ctx.send(ctx.message.author.name + " a été ajouté au groupe Miage")


bot.run('NjI1OTc1MDkwNDI4NzcyMzUy.XYo0Vg.Pk1A5sqbFuRR9JpoWnPnJbceRcA')